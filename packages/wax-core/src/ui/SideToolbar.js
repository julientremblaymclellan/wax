import { Component, Toolbar } from 'substance'

class SideToolbar extends Component {
  render($$) {
    const { configurator } = this.props
    const displayMenu =
      configurator.menus && configurator.menus.sideToolBar
        ? configurator.menus.sideToolBar
        : 'sideDefault'

    const wrapper = $$('div').addClass('sidenav')
    const sideNav = $$('div')
      .addClass('se-toolbar-wrapper')
      .append(
        $$(Toolbar, {
          toolPanel: configurator.getToolPanel(displayMenu),
        }),
      )
    return wrapper.append(sideNav)
  }
}

export default SideToolbar
