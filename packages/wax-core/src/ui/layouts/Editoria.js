import {
  Component,
  // ContextMenu,
  Overlay,
  ScrollPane,
  SplitPane,
} from 'substance'

import Comments from '../../panes/Comments/CommentBoxList'
import ContainerEditor from '../../ContainerEditor'
import DiacriticsModal from '../../elements/diacritics/DiacriticsModal'
import FindAndReplaceModal from '../../elements/find_and_replace/FindAndReplaceModal'
import ShortCutsModal from '../../elements/shortcuts_modal/ShortCutsModal'
import ModalWarning from '../../elements/modal_warning/ModalWarning'
import Notes from '../../panes/Notes/Notes'
import SideToolbar from '../SideToolbar'
import Toolbar from '../Toolbar'

import './editoria.scss'

/*
  TODO -- refactor so that as much as default is re-used here
*/

class EditoriaLayout extends Component {
  render($$) {
    const {
      chapterNumber,
      changesNotSaved,
      commandStates,
      configurator,
      autoSave,
      containerId,
      editing,
      diacriticsModal,
      findAndReplaceModal,
      shortCutsModal,
      editorSession,
      onClose,
      spellCheck,
      trackChanges,
      trackChangesView,
      theme,
      update,
      user,
      tools,
    } = this.props

    const el = $$('div')

    // top toolbar
    const toolbar = $$(Toolbar, {
      commandStates,
      disabled: editing !== 'full',
      trackChanges,
      trackChangesView,
      configurator,
      tools,
    }).ref('toolbar')

    // left toolbar
    // TODO -- refactor sideToolbar and sideNav into one
    const sideToolbar = $$(SideToolbar, {
      configurator,
    }).ref('toolbarLeft')

    let chapterComponent = $$('div')
    if (chapterNumber) {
      const chapterNumberContent = `Chapter ${chapterNumber}`
      chapterComponent = $$('div')
        .append(chapterNumberContent)
        .addClass('sc-chapter-number')
    }

    // surface
    const editor = $$(ContainerEditor, {
      containerId: 'main',
      editing,
      disabled: false,
      editorSession,
      autoSave,
      trackChanges,
      textCommands: ['full'],
    })
      .addClass(` wax-e-${theme}`)
      .ref('main')

    // notes pane
    const footerNotes = $$(Notes, {
      editing,
      trackChanges,
      trackChangesView,
      configurator,
      update,
      user,
    }).ref('footer-notes')

    const commentsPane = $$(Comments, {
      user,
      containerType: 'container',
      containerId: this.props.containerId,
    }).addClass('sc-comments-pane')

    const editorWithChapter = $$(SplitPane, {
      splitType: 'horizontal',
    }).append(
      chapterComponent,
      editor, // editorWithChapter,
    )
    // editor & comments split pane
    const editorWithComments = $$(SplitPane, {
      sizeA: '50%',
      splitType: 'vertical',
    }).append(editorWithChapter, commentsPane)

    // append overlay to editor/comments split pane
    const contentPanel = $$(ScrollPane, {
      name: 'contentPanel',
      scrollbarPosition: 'right',
    })
      .append(
        editorWithComments,
        $$(Overlay, {
          toolPanel: configurator.getToolPanel('main-overlay'),
          theme: 'dark',
        }),
      )
      .attr('id', `content-panel-${containerId}`)
      .ref('contentPanel')

    // editor/comments & side toolbar split pane
    const editorWithSidenav = $$(SplitPane, {
      sizeA: '16%',
      splitType: 'vertical',
    }).append(sideToolbar, contentPanel)

    // TODO -- remove this, as the div is only used to give space to the right
    const contentPanelWithSplitPane = $$(SplitPane, {
      sizeA: '95%',
      splitType: 'vertical',
    }).append(editorWithSidenav, $$('div'))

    // horizontal split pane with the toolbar and editor/comments/sidemenu
    const ToolbarWithEditor = $$(SplitPane, {
      splitType: 'horizontal',
    }).append(toolbar, contentPanelWithSplitPane)

    // editor/comments/sidebar/toolbar & notes pane
    const editorWithNotes = $$(SplitPane, {
      sizeA: '100%',
      splitType: 'horizontal',
    }).append(ToolbarWithEditor, footerNotes)

    // attach all to the element
    el.append(editorWithNotes)

    // TODO -- centralize modals
    // unsaved changes modal
    const unsavedChangesModal = $$(ModalWarning, {
      action: '',
      location: '',
      onClose,
      textAlign: 'center',
      width: 'medium',
    }).ref('modal-warning')

    if (changesNotSaved) {
      el.append(unsavedChangesModal)
    }

    if (diacriticsModal) {
      el.append($$(DiacriticsModal))
    }

    if (findAndReplaceModal) {
      el.append($$(FindAndReplaceModal))
    }

    if (shortCutsModal) {
      el.append($$(ShortCutsModal))
    }

    return el
  }
}

export default EditoriaLayout
