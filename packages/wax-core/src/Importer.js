import { forEach } from 'lodash'
import { HTMLImporter, ArrayIterator } from 'substance'

import Article from './Article'

class Importer extends HTMLImporter {
  constructor(config) {
    super({
      schema: config.schema,
      converters: config.converters,
      DocumentClass: Article,
    })
  }

  setOptions() {
    const options = {
      preserveWhitespace: true,
    }
    Object.assign(this.state, options)
  }

  // Takes an HTML string.
  convertDocument(bodyEls) {
    this.setOptions()
    bodyEls = bodyEls.find('body')
    if (!bodyEls) return false

    if (!bodyEls.length) bodyEls = [bodyEls]

    forEach(bodyEls[0].childNodes, bodyEl => {
      // XSweet conversion ends up with "\n" between tags (ex <!DOCTYPE HTML>\n<html)
      // which get into nodes, so filter that out.
      if (typeof bodyEl.tagName === 'undefined') return
      if (bodyEl.el.tagName === 'DIV') {
        forEach(bodyEl.childNodes, (element, order) => {
          this.convertContainer(
            element.childNodes,
            element.el.id,
            element.el.tagName.toLowerCase(),
            order + 1,
          )
        })
      } else {
        this.convertContainer(
          bodyEl.childNodes,
          bodyEl.el.id,
          bodyEl.el.tagName.toLowerCase(),
        )
      }
    })
  }

  convertContainer(elements, containerId, containerType, order = 0) {
    if (!this.state.doc) this.reset()
    const state = this.state
    const iterator = new ArrayIterator(elements)
    const nodeIds = []
    while (iterator.hasNext()) {
      const el = iterator.next()
      let node
      const blockTypeConverter = this._getConverterForElement(el, 'block')
      if (blockTypeConverter) {
        state.pushContext(el.tagName, blockTypeConverter)
        let nodeData = this._createNodeData(el, blockTypeConverter.type)
        nodeData = blockTypeConverter.import(el, nodeData, this) || nodeData
        node = this._createNode(nodeData)
        let context = state.popContext()
        context.annos.forEach(a => {
          this._createNode(a)
        })
      } else if (el.isCommentNode()) {
        continue
      } else {
        // skip empty text nodes
        if (el.isTextNode() && /^\s*$/.exec(el.textContent)) continue
        // If we find text nodes on the block level we wrap
        // it into a paragraph element (or what is configured as default block level element)
        iterator.back()
        node = this._wrapInlineElementsIntoBlockElement(iterator)
      }
      if (node) {
        nodeIds.push(node.id)
      }
    }
    const container = {
      type: containerType,
      id: containerId,
      order,
      nodes: nodeIds,
    }
    return this._createNode(container)
  }

  // TODO -- check substance's implementation of overlapping annotations

  // override substance's internal function to allow for overlapping
  // annotations, without adhering to an expand / fuse mode
  _createInlineNodes() {
    const state = this.state
    const doc = state.doc

    /*
      substance will break overlapping annotations of the same type into
      pieces like this:

      <anno id=1>
          <anno id=2></anno>
      </anno>
      <anno id=2>
      </anno>

      when the importer finds the duplicate annotation id, it will remove
      the first one altogether as a node from the doc

      here, we are forcing it to keep these duplicates and merge them
    */

    state.inlineNodes.forEach(node => {
      if (doc.get(node.id)) {
        const existing = doc.get(node.id)
        const newOne = node

        doc.delete(node.id)

        if (
          existing.start.offset === newOne.end.offset ||
          existing.end.offset === newOne.start.offset
        ) {
          node.start.offset = Math.min(
            existing.start.offset,
            newOne.start.offset,
          )
          node.end.offset = Math.max(existing.end.offset, newOne.end.offset)

          doc.create(node)
        }
        return
      }
      doc.create(node)
    })
  }
}

export default Importer
