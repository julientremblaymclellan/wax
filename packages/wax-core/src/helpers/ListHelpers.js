import { cloneDeep, each, find, findIndex, isEmpty, last } from 'lodash'

const containsImage = (params, selection) => {
  const nodes = getNodesForSelection(params, selection)
  return !isEmpty(find(nodes, { type: 'image' }))
}

const containsList = (params, selection) => {
  const nodes = getNodesForSelection(params, selection)
  return !isEmpty(find(nodes, { type: 'list' }))
}

const createContainerSelection = (
  params,
  selection,
  tx,
  startPath,
  endPath,
) => {
  const { containerId } = transactionHelpers(params)

  return tx.createSelection({
    containerId,
    endPath: [endPath, 'content'],
    endOffset: selection.end.offset,
    startPath: [startPath, 'content'],
    startOffset: selection.start.offset,
    type: 'container',
  })
}

const toggleList = (tx, params, options) => {
  const { listName } = options.config
  const { listType } = options.config.spec || 'bullet'

  tx.toggleList({ listType })
}

const getContainer = params => {
  const { containerId, doc } = transactionHelpers(params)
  return doc.get(containerId)
}

const getDefaultTextType = tx => {
  const schema = tx.getSchema()
  return schema.getDefaultTextType()
}

const getFirstSelectedListEntry = (params, selection) => {
  const { doc } = transactionHelpers(params)

  const firstListItemSelected = doc.get(selection.start.path[0])
  return firstListItemSelected
}

const getLastSelectedListEntry = (params, selection) => {
  const { doc } = transactionHelpers(params)

  const lastListItemSelected = doc.get(selection.end.path[0])
  return lastListItemSelected
}

const getLists = (params, selection) => {
  const { doc } = transactionHelpers(params)
  const nodes = getNodesForSelection(params, selection)
  const lists = []

  nodes.forEach(node => {
    if (node.type === 'list') lists.push(doc.get(node.id))
  })

  return lists
}

const getNodesForSelection = (params, selection) => {
  const container = getContainer(params)
  const firstNodePosition = container.getPosition(selection.start.path[0])
  const lastNodePosition = container.getPosition(selection.end.path[0])
  const nodesContained = []

  for (let i = firstNodePosition; i <= lastNodePosition; i += 1) {
    const node = container.getNodeAt(i)
    const obj = { type: node.type, id: node.id }

    nodesContained.push(obj)
  }

  return nodesContained
}

const isListItem = (params, nodeId) => {
  const { doc } = transactionHelpers(params)
  const node = doc.get(nodeId)

  if (node.type === 'list-item') return true
  return false
}

const sliceListsAccordingToSelection = (params, selection) => {
  const nodes = getNodesForSelection(params, selection)
  const lists = getLists(params, selection)
  const copyLists = cloneDeep(lists)

  const isStartSelectionAListItem = isListItem(params, selection.start.path[0])
  let firstListEntry = null

  if (isStartSelectionAListItem) {
    firstListEntry = getFirstSelectedListEntry(params, selection)
  }

  if (firstListEntry && last(nodes).type !== 'list') {
    copyLists.forEach(list => {
      if (firstListEntry.parent.id === list.id) {
        const arrayIndex = findIndex(list.items, value => {
          return value === firstListEntry.id
        })
        list.items.splice(0, arrayIndex)
      }
    })
  }

  if (last(nodes).type === 'list') {
    const lastListEntry = getLastSelectedListEntry(params, selection)

    copyLists.forEach(list => {
      if (
        firstListEntry &&
        firstListEntry.parent.id === list.id &&
        lastListEntry.parent.id === list.id
      ) {
        const firstIndex = findIndex(
          list.items,
          value => value === firstListEntry.id,
        )

        const lastIndex = findIndex(
          list.items,
          value => value === lastListEntry.id,
        )

        list.items.splice(lastIndex + 1, list['items'].length)
        list.items.splice(0, firstIndex)
      } else if (firstListEntry && firstListEntry.parent.id === list.id) {
        const arrayIndex = findIndex(
          list.items,
          value => value === firstListEntry.id,
        )
        list.items.splice(0, arrayIndex)
      } else if (lastListEntry.parent.id === list.id) {
        const arrayIndex = findIndex(
          list.items,
          value => value === lastListEntry.id,
        )
        list.items.splice(arrayIndex + 1)
      }
    })
  }

  return copyLists
}

const transactionHelpers = params => {
  const { editorSession, surface } = params
  const { containerId } = surface

  const doc = editorSession.getDocument()
  const selection = editorSession.getSelection()

  return {
    containerId,
    doc,
    editorSession,
    selection,
    surface,
  }
}

const transformListToParagraphs = (params, tx, lists, options) => {
  const { containerId } = transactionHelpers(params)
  const paragraphIds = []

  lists.forEach(list => {
    list.items.forEach((item, index) => {
      tx.setSelection({
        type: 'property',
        path: [item, 'content'],
        startOffset: 0,
        endOffset: 0,
        containerId,
      })

      toggleList(tx, params, options)

      const defaultTextType = getDefaultTextType(tx)
      const paragraph = tx.switchTextType({ type: defaultTextType })
      paragraphIds.push(paragraph.id)
    })
  })

  return paragraphIds
}

export {
  containsList,
  containsImage,
  createContainerSelection,
  getFirstSelectedListEntry,
  getLastSelectedListEntry,
  getNodesForSelection,
  getContainer,
  getLists,
  isListItem,
  sliceListsAccordingToSelection,
  toggleList,
  transactionHelpers,
  transformListToParagraphs,
}
