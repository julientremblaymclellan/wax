export default {
  configure: config => {
    name: 'ucpToolPanels',
      config.addToolPanel('ucpTopToolBar', [
        {
          name: 'undo-redo',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['undo-redo'],
        },
        {
          name: 'persistence',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['persistence'],
        },
        {
          name: 'change-case',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['change-case'],
        },
        {
          name: 'annotations',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['annotations'],
        },
        {
          name: 'insert-image',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['insert-image'],
        },
        {
          name: 'note',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['note'],
        },
        {
          name: 'diacritics-tool',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['diacritics-tool'],
        },
        {
          name: 'find-and-replace-tool',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['find-and-replace-tool'],
        },
        {
          name: 'highlighter',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['highlighter'],
        },
        {
          name: 'ornament',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['ornament'],
        },
        {
          name: 'spell-check-toggle',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['spell-check-toggle'],
        },
        {
          name: 'full-screen-control',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['full-screen-control'],
        },
        {
          name: 'shortcuts-modal',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['shortcuts-modal'],
        },
        {
          name: 'track-change-controls',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['track-change-controls'],
        },
        {
          name: 'table',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['table'],
        },
      ])

    config.addToolPanel('ucpSideToolBarFront', [
      {
        name: 'text-display-front',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['front-matter-a'],
      },
      {
        name: 'series-list',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list-series'],
      },
      {
        name: 'text-display-front-b',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['front-matter-b'],
      },
      {
        name: 'abbreviations-list',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list-abbreviations'],
      },
      {
        name: 'text-display-front-c',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['front-matter-c'],
      },
    ])

    config.addToolPanel('ucpSideToolBarBodyPart', [
      {
        name: 'text-display',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-display-part'],
      },
      {
        name: 'text-types',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-types-part'],
      },
      {
        name: 'list',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list-part-chapter'],
      },
    ])

    config.addToolPanel('ucpSideToolBarBodyChapter', [
      {
        name: 'text-display',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-display-chapter'],
      },
      {
        name: 'text-types',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-types-chapter'],
      },
      {
        name: 'list-part-chapter',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list-part-chapter'],
      },
      {
        name: 'list',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list'],
      },
    ])

    config.addToolPanel('ucpSideToolBarBack', [
      {
        name: 'text-types',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-display-back'],
      },
      {
        name: 'text-display',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['back-matter-a'],
      },
      {
        name: 'abbreviations-list',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['ucp-list-abbreviations'],
      },
    ])

    config.addToolPanel('main-overlay', [
      {
        name: 'comment',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['comment'],
      },
      {
        name: 'prompt',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['prompt'],
      },
      {
        name: 'spell-check',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['spell-check'],
      },
      {
        name: 'caption',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['caption'],
      },
    ])
  },
}
