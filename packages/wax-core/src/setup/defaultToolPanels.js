export default {
  configure: config => {
    name: 'defaultToolPanels',
      config.addToolPanel('topDefault', [
        {
          name: 'undo-redo',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['undo-redo'],
        },
        {
          name: 'persistence',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['persistence'],
        },
        {
          name: 'change-case',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['change-case'],
        },
        {
          name: 'insert-strong',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['insert-strong'],
        },
        {
          name: 'annotations',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['annotations'],
        },
        {
          name: 'insert-inline-code',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['insert-inline-code'],
        },
        {
          name: 'insert-image',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['insert-image'],
        },
        {
          name: 'note',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['note'],
        },
        {
          name: 'list',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['list'],
        },
        {
          name: 'diacritics-tool',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['diacritics-tool'],
        },
        {
          name: 'find-and-replace-tool',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['find-and-replace-tool'],
        },
        {
          name: 'highlighter',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['highlighter'],
        },
        {
          name: 'ornament',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['ornament'],
        },
        {
          name: 'spell-check-toggle',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['spell-check-toggle'],
        },
        {
          name: 'insert-script',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['insert-script'],
        },
        {
          name: 'full-screen-control',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['full-screen-control'],
        },
        {
          name: 'shortcuts-modal',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['shortcuts-modal'],
        },
        {
          name: 'track-change-controls',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['track-change-controls'],
        },
        {
          name: 'table',
          type: 'tool-group',
          showDisabled: true,
          style: 'minimal',
          commandGroups: ['table'],
        },
      ])

    config.addToolPanel('sideDefault', [
      {
        name: 'text-display',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-display-chapter'],
      },
      {
        name: 'text-types',
        type: 'tool-group',
        showDisabled: true,
        style: 'descriptive',
        commandGroups: ['text-types-chapter'],
      },
    ])

    config.addToolPanel('main-overlay', [
      {
        name: 'comment',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['comment'],
      },
      {
        name: 'prompt',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['prompt'],
      },
      {
        name: 'spell-check',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['spell-check'],
      },
      {
        name: 'caption',
        type: 'tool-prompt',
        showDisabled: false,
        commandGroups: ['caption'],
      },
    ])
  },
}
