import { first, last } from 'lodash'
import { uuid } from 'substance'
import ContainerEditor from '../../ContainerEditor'

class NotesContainerEditor extends ContainerEditor {
  focus() {
    const notesProvider = this.context.notesProvider
    const active = notesProvider.getActiveNote()
    if (this.containerId !== `container-${active}`) {
      setTimeout(() => {
        notesProvider.setActiveNote(this.props.noteParentId)
      }, 200)
    }
  }

  isAtEndOfNote() {
    const doc = this.context.editorSession.getDocument()
    const selection = this.context.editorSession.getSelection()
    const path = selection.path[0]
    const containerId = selection.surfaceId
    const container = doc.get(containerId)
    const lastParagraph = last(container.nodes)
    const lastParaphLength = doc.get(lastParagraph).getLength()
    if (path === lastParagraph && lastParaphLength === selection.end.offset) {
      return true
    }

    return false
  }

  isAtStartOfNote() {
    const doc = this.context.editorSession.getDocument()
    const selection = this.context.editorSession.getSelection()
    const containerId = selection.containerId
    const container = doc.get(containerId)
    const path = selection.path[0]
    const firstParagraph = first(container.nodes)

    if (path === firstParagraph && selection.end.offset === 0) {
      return true
    }

    return false
  }

  getContainerSurface() {
    const doc = this.context.editorSession.getDocument()
    const selection = this.context.editorSession.getSelection()
    const containerId = selection.containerId
    const container = doc.get(containerId)
    return container
  }

  _handleLeftOrRightArrowKey(event) {
    if (event.code === 'ArrowRight') {
      if (this.isAtEndOfNote()) {
        const container = this.getContainerSurface()
        this.moveToNext(container)
      } else {
        super._handleLeftOrRightArrowKey(event)
      }
    } else if (event.code === 'ArrowLeft') {
      if (this.isAtStartOfNote()) {
        const container = this.getContainerSurface()
        this.moveToPrevious(container)
      } else {
        super._handleLeftOrRightArrowKey(event)
      }
    }
  }

  _handleUpOrDownArrowKey(event) {
    super._handleUpOrDownArrowKey(event)
    const doc = this.context.editorSession.getDocument()

    if (event.code === 'ArrowDown') {
      // setTimeout exists in order to get the new cursor position,
      // because subtance updates the selection
      // in a setTimeout
      setTimeout(() => {
        if (this.isAtEndOfNote()) {
          const container = this.getContainerSurface()
          this.moveToNext(container)
        }
      })
    } else if (event.code === 'ArrowUp') {
      setTimeout(() => {
        if (this.isAtStartOfNote()) {
          const container = this.getContainerSurface()
          this.moveToPrevious(container)
        }
      })
    }
  }

  moveToNext(container) {
    const provider = this.getProvider()
    const doc = this.context.editorSession.getDocument()
    const notes = provider.entries

    if (container.order === notes.length) return

    const note = notes[container.order]
    const nextContainer = doc.get(`container-${note.id}`)

    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerid: `container-${note.id}`,
        surfaceId: `container-${note.id}`,
        path: [first(nextContainer.nodes), 'content'],
        startOffset: 0,
        endOffset: 0,
      })
    })
  }

  moveToPrevious(container) {
    const provider = this.getProvider()
    const doc = this.context.editorSession.getDocument()
    const notes = provider.entries

    if (container.order === 1) return

    const note = notes[container.order - 2]
    provider.setActiveNote(note.id)

    const previousContainer = doc.get(`container-${note.id}`)
    const lastParagraph = doc.get(last(previousContainer.nodes))

    setTimeout(() => {
      this.context.editorSession.transaction(tx => {
        tx.setSelection({
          type: 'property',
          containerid: `container-${note.id}`,
          surfaceId: `container-${note.id}`,
          startOffset: lastParagraph.getLength(),
          endOffset: lastParagraph.getLength(),
          path: [lastParagraph.id, 'content'],
        })
      })
    })
  }

  getProvider() {
    return this.context.notesProvider
  }
}

export default NotesContainerEditor
