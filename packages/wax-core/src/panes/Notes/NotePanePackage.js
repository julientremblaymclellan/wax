import NoteContainer from './NoteContainer'

export default {
  name: 'note-container',
  configure: config => {
    config.addNode(NoteContainer)
  },
}
