import { first, findKey, forEach } from 'lodash'
import { Component } from 'substance'
import NotesContainerEditor from './NotesContainerEditor'

class NoteContainerComponent extends Component {
  didMount() {
    const provider = this.getProvider()
    provider.on('active:changed', this.rerender, this)
  }

  render($$) {
    const provider = this.getProvider()
    const entries = provider.entries
    const activeNote = provider.getActiveNote()
    const viewMode = this.getViewMode()
    const disabled = this.props.disabled
    const el = $$('div').addClass('container-note')

    const computedEntries = viewMode ? entries : this.excludeHidden(entries)

    const keyFound = findKey(
      computedEntries,
      note => note.id === this.props.noteParentId,
    )
    const index = parseInt(keyFound, 10) + 1

    const numberLaber = $$('span')
      .append(index)
      .append('.')
      .addClass('note-number')
      .on('click', this.scrollToCallout)
      .attr('data-clickable', true)

    const spellCheck = this.context.editor.state.spellCheck

    const container = $$(NotesContainerEditor, {
      containerId: this.props.containerId,
      disabled,
      editing: this.props.editing,
      noteParentId: this.props.noteParentId,
      spellcheck: spellCheck,
      trackChanges: this.props.trackChanges,
      textCommands: ['emphasis', 'diacritics', 'comment'],
    }).ref(this.props.containerId)

    if (disabled) {
      el.addClass('note-track-deleted')
    }

    if (viewMode === false && disabled) {
      el.addClass('sc-track-note-hide')
    }

    if (activeNote === this.props.noteParentId) {
      el.addClass('active-note')
      this.setCursorFocus()
    }

    return el.append(numberLaber, container).attr('data-clickable', true)
  }

  setCursorFocus() {
    const editorSession = this.context.editorSession
    const doc = editorSession.document
    const nodes = doc.getNodes()
    const container = nodes[this.props.containerId]
    const containerNodes = container.nodes

    setTimeout(() => {
      const containerId = editorSession.getSelection().containerId
      if (containerId === this.props.containerId) return

      this.context.editorSession.transaction(tx => {
        tx.setSelection({
          type: 'property',
          containerId: this.props.containerId,
          surfaceId: this.props.containerId,
          startOffset: 0,
          endOffset: 0,
          path: [first(containerNodes), 'content'],
        })
      })
    })
  }

  scrollToCallout() {
    this.context.editor.scrollTo(`[data-id="${this.props.noteParentId}"]`)
    this.setActive(this.props.noteParentId)
  }

  setActive(noteId) {
    const provider = this.getProvider()
    provider.setActiveNote(noteId)
  }

  getProvider() {
    return this.context.notesProvider
  }

  getViewMode() {
    const editor = this.getEditor()
    const { trackChangesView } = editor.state
    return trackChangesView
  }

  getEditor() {
    return this.context.editor
  }

  excludeHidden(entries) {
    const filteredEntries = []
    forEach(entries, entry => {
      if (!entry.node.disabled) {
        filteredEntries.push(entry)
      }
    })
    return filteredEntries
  }
}

export default NoteContainerComponent
