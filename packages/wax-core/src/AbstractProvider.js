import {
  clone,
  eachRight,
  find,
  forEach,
  includes,
  isEmpty,
  map,
  pickBy,
  sortBy,
  last,
  first,
  findIndex,
  maxBy,
} from 'lodash'

import { EventEmitter } from 'substance'

class AbstractProvider extends EventEmitter {
  constructor(document, config) {
    super(document, config)
    this.document = document
    this.config = config
    this.editorSession = config.editorSession
    this.entries = this.computeEntries()

    if (this.entries.length > 0) {
      this.activeEntry = this.entries[0].id
    } else {
      this.activeEntry = null
    }
  }

  dispose() {
    const doc = this.getDocument()
    doc._disconnect(this)
  }

  // Inspects a document change and recomputes the
  // entries if necessary
  handleDocumentChange(change) {
    console.warn('this method is Abstract')
  }

  computeEntries() {
    const doc = this.getDocument()
    const nodes = doc.getNodes()
    const ref = this
    const allNodes = {}
    // get all nodes from the document
    forEach(ref.constructor.NodeType, type => {
      const typeNodes = pickBy(nodes, (value, key) => value.type === type)
      Object.assign(allNodes, typeNodes)
    })
    return this.sortNodes(allNodes)
  }

  // TODO FIX THIS MESS
  sortNodes(nodes) {
    let currentNodes = clone(nodes)
    const containers = this.getAllAvailableSurfaces()

    // sort notes by
    // the index of the containing block
    // their position within that block
    currentNodes = map(currentNodes, node => {
      const blockId = node.path[0]
      let container = containers.find(cont => {
        if (includes(cont.nodes, blockId)) {
          return cont
        }
      })
      // This is actually not correct.
      // TODO if container not found selection
      // might be for example a list-item
      // Now works as lists only are allowed in main surface.
      // Fix as to find the right container for the selection.
      if (!container) {
        // const containerId = this.document.get(node.path[0]).parent.parent.id
        // container = this.document.get(containerId)
        container = containers[0]
      }

      const blockPosition = this.setBlockPosition(container, blockId)
      const nodePosition = node.start.offset
      const depth = this.setNodeDepth(node, container, blockId)
      const containerOrder = container.order ? container.order : 0

      return {
        id: node.id,
        blockPosition,
        nodePosition,
        node,
        containerId: container.id,
        containerType: container.type,
        order: containerOrder,
        depth,
      }
    })

    return this.computeHash(
      sortBy(currentNodes, ['order', 'blockPosition', 'depth', 'nodePosition']),
    )
  }

  computeHash(nodes) {
    if (nodes.length === 0) return []

    const {
      maxOrder,
      maxBlockPosition,
      maxDepth,
      maxStartOffset,
    } = this.computeMaxes(nodes)

    forEach(nodes, (value, key) => {
      nodes[key].hash = `${this.pad(value.order, maxOrder)}${this.pad(
        value.blockPosition,
        maxBlockPosition,
      )}${this.pad(value.depth, maxDepth)}${this.pad(
        value.node.start.offset,
        maxStartOffset,
      )}`
    })
    return nodes
  }

  computeMaxes(nodes) {
    const maxBlockPosition = maxBy(
      clone(nodes),
      'blockPosition',
    ).blockPosition.toString().length
    const maxOrder = maxBy(clone(nodes), 'order').order.toString().length
    const maxDepth = maxBy(clone(nodes), 'depth').depth.toString().length
    const maxStartOffset = maxBy(
      clone(nodes),
      'node.start.offset',
    ).node.start.offset.toString().length
    return {
      maxOrder,
      maxBlockPosition,
      maxDepth,
      maxStartOffset,
    }
  }

  pad(num, size) {
    var s = num + ''
    while (s.length < size) s = '0' + s
    return s
  }

  sortNodesBySurface() {
    // TODO implement
  }

  getProviderNode(id) {
    const node = find(this.entries, { id })
    return node
  }

  getAllAvailableSurfaces() {
    const nodes = this.document.getNodes()
    const containers = []

    forEach(nodes, node => {
      if (node._isContainer) {
        containers.push(node)
      }
    })
    return containers
  }

  getNextNode() {
    const { editorSession } = this
    const selection = editorSession.getSelection()
    const container = this.getContainerForSelection(selection)
    const selectionBlockPositon = this.setBlockPosition(
      container,
      selection.path[0],
    )
    const selectionDepth = this.setNodeDepth(selection, container)
    const containerOrder = container.order ? container.order : 0
    const matches = this.getEntries()

    const {
      maxOrder,
      maxBlockPosition,
      maxDepth,
      maxStartOffset,
    } = this.computeMaxes(matches)

    const currentHash = `${this.pad(containerOrder, maxOrder)}${this.pad(
      selectionBlockPositon,
      maxBlockPosition,
    )}${this.pad(selectionDepth, maxDepth)}${this.pad(
      selection.start.offset,
      maxStartOffset,
    )}`

    let found = undefined
    forEach(matches, match => {
      if (currentHash < match.hash) {
        found = match
        return false
      }
    })
    if (found) return found
    return first(matches)
  }

  getPreviousNode() {
    const { editorSession } = this
    const selection = editorSession.getSelection()
    const container = this.getContainerForSelection(selection)
    const selectionBlockPositon = this.setBlockPosition(
      container,
      selection.path[0],
    )
    const selectionDepth = this.setNodeDepth(selection, container)
    const containerOrder = container.order ? container.order : 0
    const matches = this.getEntries()

    const {
      maxOrder,
      maxBlockPosition,
      maxDepth,
      maxStartOffset,
    } = this.computeMaxes(matches)

    const currentHash = `${this.pad(containerOrder, maxOrder)}${this.pad(
      selectionBlockPositon,
      maxBlockPosition,
    )}${this.pad(selectionDepth, maxDepth)}${this.pad(
      selection.start.offset,
      maxStartOffset,
    )}`

    let found = undefined
    eachRight(matches, match => {
      if (parseInt(currentHash) > parseInt(match.hash)) {
        found = match
        return false
      }
    })

    if (found) return found
    return last(matches)
  }

  setBlockPosition(container, blockId) {
    let blockPosition = container.getPosition(blockId)
    if (container.id.includes('caption')) {
      const mainSurface = this.getMainSurface()
      blockPosition = mainSurface.getPosition(container.id.substring(8))
    }
    return blockPosition
  }

  setNodeDepth(node, container, blockId) {
    let depth = 0
    if (container.id.includes('caption')) {
      depth = container.getPosition(node.path[0]) + 1
    }
    if (node.path[0].includes('list-item')) {
      let list = this.document.get(node.path[0])

      if (list.parent) {
        list = list.parent
      } else {
        //TODO Find actual List as on undo substance
        // return a different object
        return depth
      }
      depth = findIndex(list.items, value => value === node.path[0]) + 1
    }
    return depth
  }

  getContainerForSelection(selection) {
    const containersEls = this.getAllAvailableSurfaces(this.document)
    const selectionNodeId = selection.getNodeId()

    let container = containersEls.find(cont => {
      if (includes(cont.nodes, selectionNodeId)) {
        return cont
      }
    })
    if (!container) container = containersEls[0]
    return container
  }

  hasEntries() {
    return !isEmpty(this.getEntries())
  }

  getEntries() {
    return this.computeEntries()
  }

  getEntriesLengthByType(type) {
    const doc = this.document
    const nodes = doc.getNodes()
    const allNodes = pickBy(nodes, (value, key) => value.type === type)

    return Object.keys(allNodes).length
  }

  getDocument() {
    return this.document
  }

  getActiveSurface() {
    return this.editorSession.surfaceManager.getFocusedSurface()
  }

  getMainSurface() {
    return this.editorSession.document.get('main')
  }
}

export default AbstractProvider
