import { FontAwesomeIcon as Icon } from 'substance'
import { filter } from 'lodash'
import Modal from '../modal/Modal'
import DiacriticsList from './DiacriticsList'
import InsertMultiple from './InsertMultiple'
import SearchField from './SearchField'
import SpecialCharacters from './SpecialCharacters'

class DiacriticsModal extends Modal {
  constructor(...props) {
    super(...props)
    this.insertChar = this.insertChar.bind(this)
    this.filterIcons = this.filterIcons.bind(this)
    this.insertMultipleChar = this.insertMultipleChar.bind(this)
    this.insertMultiple = false
  }

  render($$) {
    const el = $$('div').addClass('sc-modal-diacritics')

    const closeButton = $$(Icon, { icon: 'fa-close' })
      .addClass('sc-close-modal')
      .on('click', this.closeModal)

    const modalHeader = $$('div')
      .addClass('sc-modal-header')
      .append(closeButton)

    const searchField = $$(SearchField, {
      onKeyUp: this.filterIcons,
    }).ref('search-field')

    const diacritics = $$(DiacriticsList, {
      onClick: this.insertChar,
      editorSession: this.context.editorSession,
      specialCharacters: this.state.specialCharacters || SpecialCharacters,
    })

    const insertMultiple = $$(InsertMultiple, {
      insertMultipleChar: this.insertMultipleChar,
    }).ref('insert-multiple')

    const modalBody = $$('div')
      .addClass('sc-modal-body')
      .append(searchField)
      .append(diacritics)
      .append(insertMultiple)

    el.append(
      $$('div')
        .addClass('se-body-diacritics')
        .append(modalHeader)
        .append(modalBody),
    )

    return el
  }

  insertMultipleChar() {
    const element = document.getElementById('multiple-char')
    this.insertMultiple = element.checked
  }

  insertChar(char) {
    const { editor } = this.context
    const { trackChanges } = editor.props
    const { user } = editor.props
    const selection = this.context.editorSession.getSelection()

    if (trackChanges) {
      this.context.editorSession.transaction(
        (tx, args) => {
          tx.insertText(char)
          tx.create({
            status: 'add',
            type: 'track-change',
            path: selection.start.path,
            startOffset: selection.start.offset,
            endOffset: selection.end.offset + 1,
            user,
          })
        },
        { action: 'type' },
      )
      return
    }
    this.context.editorSession.transaction(
      (tx, args) => {
        tx.insertText(char)
      },
      { action: 'type' },
    )

    if (!this.insertMultiple) this.closeModal()
  }

  filterIcons(event) {
    const element = document.getElementById('search-diacrtics')
    const searchTerm = element.value.toLowerCase()

    const filtertedSpecialCharacters = filter(SpecialCharacters, value => {
      if (value.name.toLowerCase().includes(searchTerm)) return value.name
      return false
    })

    const list =
      element.value.length === 0
        ? SpecialCharacters
        : filtertedSpecialCharacters

    this.extendState({
      specialCharacters: list,
    })
  }

  closeModal() {
    this.context.editor.emit('toggleModal', 'diacriticsModal')
  }

  getInitialState() {
    return {
      specialCharacters: SpecialCharacters,
    }
  }
}

export default DiacriticsModal
