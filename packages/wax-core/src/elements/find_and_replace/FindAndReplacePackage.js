import { platform } from 'substance'

import FindAndReplace from './FindAndReplace'
import FindAndReplaceComponent from './FindAndReplaceComponent'
import FindAndReplaceCommand from './FindAndReplaceCommand'
import FindAndReplaceControlTool from './FindAndReplaceControlTool'
import FindAndReplaceHTMLConverter from './FindAndReplaceHTMLConverter'
import FindAndReplaceProvider from './FindAndReplaceProvider'

//TODO break into seperate commands in order to have keyBoardShortcuts

// import FindNextCommand from './FindNextCommand'
// import FindPreviousCommand from './FindPreviousCommand'
// import ReplaceAllCommand from './ReplaceAllCommand'
// import ReplaceNextCommand from './ReplaceNextCommand'

export default {
  name: 'find-and-replace',
  configure: (config, { toolGroup }) => {
    config.addNode(FindAndReplace)
    config.addComponent(FindAndReplace.type, FindAndReplaceComponent)
    config.addConverter('html', FindAndReplaceHTMLConverter)

    config.addTool('find-and-replace-tool', FindAndReplaceControlTool)

    config.addCommand('find-and-replace-tool', FindAndReplaceCommand, {
      commandGroup: 'find-and-replace-tool',
    })
    config.addIcon('find-and-replace-tool', { fontawesome: ' fa-search' })
    config.addLabel('find-and-replace-tool', {
      en: 'find And Replace',
    })

    config.addProvider('findAndReplaceProvider', FindAndReplaceProvider)

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'

    config.addKeyboardShortcut(`${controllerKey}+f`, {
      description: 'Find And Replace',
      command: 'find-and-replace-tool',
    })
  },
}
