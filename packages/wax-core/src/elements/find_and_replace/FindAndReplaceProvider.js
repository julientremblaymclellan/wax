import {
  each,
  includes,
  pickBy,
  forEach,
  isEmpty,
  last,
  eachRight,
} from 'lodash'
import { documentHelpers, Marker } from 'substance'
import AbstractProvider from '../../AbstractProvider'
import TrackChange from '../annotations/track_change/TrackChange'

class FindAndReplaceProvider extends AbstractProvider {
  static get NodeType() {
    return ['find-and-replace']
  }

  static initialize(context) {
    const config = {
      commandManager: context.commandManager,
      containerId: context.props.containerId,
      controller: context,
      editorSession: context.editorSession,
      surfaceManager: context.surfaceManager,
      user: context.props.user,
    }
    return new FindAndReplaceProvider(context.doc, config)
  }

  constructor(doc, props) {
    super(doc, props)

    doc.on('document:changed', this.handleDocumentChange, this)
  }

  handleDocumentChange(change) {
    if (!this.config.controller.state.findAndReplaceModal) return
    this.entries = this.computeEntries()
  }

  findInTextProperty({ path, containerId, findString, editorSession }) {
    const doc = editorSession.document
    const text = doc.get(path)

    let match
    const allMatches = []
    const matcher = new RegExp(findString, 'ig')

    do {
      match = matcher.exec(text)
      if (match) {
        allMatches.push(match)
      }
    } while (match)

    const matches = []
    each(allMatches, m => {
      const marker = new Marker(doc, {
        type: 'property',
        id: path[0],
        start: {
          path,
          offset: m.index,
        },
        end: {
          offset: m.index + m[0].length,
        },
      })
      let container = doc.get(containerId)
      if (!container) container = doc.get('main')

      marker.blockPosition = container.getPosition(path[0])
      marker.containerId = containerId
      matches.push(marker)
    })
    return matches
  }

  highLightMatches(editorSession, matches, doc) {
    const info = { hidden: true }
    this.clearMatches(doc, editorSession)
    if (matches.length === 0) return
    editorSession.transaction(tx => {
      each(matches, match => {
        tx.create({
          type: 'find-and-replace',
          start: match.start,
          end: match.end,
          path: match.path,
          containerId: match.containerId,
          surfaceId: match.containerId,
        })
      })
    }, info)
  }

  clearMatches(doc, editorSession) {
    const nodes = doc.getNodes()

    const typeNodes = pickBy(
      nodes,
      (value, key) => value.type === 'find-and-replace',
    )
    const info = { hidden: true }

    editorSession.transaction(tx => {
      each(typeNodes, node => {
        tx.delete(node.id)
      })
    }, info)
  }

  createSelection(editorSession, match) {
    const node = match.node ? match.node : match
    const containerId = match.containerId
    let surfaceId = containerId
    if (containerId.includes('caption')) {
      surfaceId = `main/${containerId.substring(8)}/${containerId}`
    }

    editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerId: containerId,
        surfaceId: surfaceId,
        path: node.path,
        startOffset: node.start.offset,
        endOffset: node.end.offset,
      })
    })
  }

  findNext() {
    const { editorSession } = this.config
    const found = this.getNextNode()
    if (found) this.createSelection(editorSession, found)
    return false
  }

  findPrevious() {
    const { editorSession } = this.config
    const found = this.getPreviousNode()
    if (found) this.createSelection(editorSession, found)
    return false
  }

  excludeTrackChanges(matches, doc) {
    eachRight(matches, (match, index) => {
      const trackChange = documentHelpers.getPropertyAnnotationsForSelection(
        doc,
        match.getSelection(),
        { type: 'track-change' },
      )

      if (trackChange.length >= 1) {
        matches.splice(index, 1)
      }
    })
    return matches
  }

  creteTrackDeletionAndInsert(nodes, replaceValue, user, editorSession) {
    editorSession.transaction(tx => {
      eachRight(nodes, node => {
        tx.create({
          status: 'delete',
          type: 'track-change',
          containerId: node.containerId,
          surfaceId: node.containerId,
          path: node.path,
          start: node.start,
          end: node.end,
          user: {
            id: user.id,
            username: user.username,
          },
        })

        TrackChange.autoExpandRight = false

        tx.setSelection({
          type: 'property',
          containerId: node.containerId,
          surfaceId: node.containerId,
          path: node.path,
          startOffset: node.end.offset,
          endOffset: node.end.offset,
        })
        tx.insertText(replaceValue)

        tx.create({
          status: 'add',
          type: 'track-change',
          containerId: node.containerId,
          surfaceId: node.containerId,
          path: node.path,
          startOffset: node.end.offset,
          endOffset: node.end.offset + replaceValue.length,
          user: {
            id: user.id,
            username: user.username,
          },
        })
      })
    })
  }
}
export default FindAndReplaceProvider
