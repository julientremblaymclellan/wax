import { TextBlockComponent } from 'substance'

class GlossaryComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-glossary')
  }
}

export default GlossaryComponent
