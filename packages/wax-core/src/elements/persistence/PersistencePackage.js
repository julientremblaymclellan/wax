import { platform } from 'substance'
import SaveTool from './SaveTool'
import SaveCommand from './SaveCommand'

export default {
  name: 'persistence',
  configure: config => {
    config.addCommand('save', SaveCommand, {
      commandGroup: 'persistence',
    })
    config.addTool('save', SaveTool)
    config.addIcon('save', { fontawesome: 'fa-save' })
    config.addLabel('save', {
      en: 'Save',
    })

    let controllerKey = 'ctrl'
    if (platform.isMac) {
      controllerKey = 'cmd'
    }

    config.addKeyboardShortcut(`${controllerKey}+s`, {
      command: 'save',
    })
  },
}
