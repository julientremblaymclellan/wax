import { Tool } from 'substance'

class SaveTool extends Tool {
  constructor(...props) {
    super(...props)
    this.context.editor.on('hightLightSave', this.hightLightSave, this)
  }

  renderButton($$) {
    const el = super.renderButton($$).css('width', '20px')
    return el
  }

  hightLightSave() {
    const saveIcon = this.el.el.childNodes[0].childNodes[0]
    saveIcon.classList.remove('fa-save')
    saveIcon.style.color = '#4990e2'
    saveIcon.classList.add('fa-check')
    saveIcon.style.transition = 'all 1s'

    setTimeout(() => {
      saveIcon.style.color = '#000'
      saveIcon.classList.remove('fa-check')
      saveIcon.classList.add('fa-save')
    }, 2000)
  }
}

export default SaveTool
