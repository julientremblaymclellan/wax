import { Command } from 'substance'

const LEFT_QUOTE = '\u201C'
const RIGHT_QUOTE = '\u201D'

class InsertQuoteMarkCommand extends Command {
  getCommandState(params, context) {
    return {
      disabled: false,
    }
  }

  execute(params, context) {
    // eslint-disable-line
    let editorSession = params.editorSession
    let sel = editorSession.getSelection()
    let doc = editorSession.getDocument()
    if (sel.isPropertySelection()) {
      let nodeId = sel.start.getNodeId()
      let node = doc.get(nodeId)
      if (node.isText()) {
        const { trackChanges } = params.surface.props
        let text = node.getText()
        let offset = sel.start.offset
        let mark
        if (offset === 0 || /\s/.exec(text.slice(offset - 1, offset))) {
          mark = LEFT_QUOTE
        } else {
          mark = RIGHT_QUOTE
        }
        if (trackChanges) {
          this.createTrackQuote(params, sel, mark)
        } else {
          editorSession.transaction(tx => {
            tx.insertText(mark)
          })
        }
        return true
      }
    }
    return false
  }

  createTrackQuote(params, sel, mark) {
    const { surface } = params
    const { user } = surface.context.editor.props
    const { containerId } = surface
    params.editorSession.transaction(tx => {
      tx.insertText(mark)
      tx.create({
        status: 'add',
        type: 'track-change',
        containerId,
        surfaceId: containerId,
        path: sel.path,
        startOffset: sel.start.offset,
        endOffset: sel.start.offset + 1,
        user: {
          id: user.id,
          username: user.username,
        },
      })
    })
  }
}

export default InsertQuoteMarkCommand
