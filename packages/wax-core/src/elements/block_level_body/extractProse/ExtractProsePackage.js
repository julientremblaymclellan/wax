import ExtractProse from './ExtractProse'
import ExtractProseComponent from './ExtractProseComponent'
import ExtractProseHTMLConverter from './ExtractProseHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'extract-prose',
  configure: config => {
    config.addNode(ExtractProse)
    config.addCommand('extract-prose-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-prose' },
      commandGroup: 'text-types-part',
    })
    config.addCommand('extract-prose-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-prose' },
      commandGroup: 'text-types-chapter',
    })
    config.addComponent(ExtractProse.type, ExtractProseComponent)
    config.addConverter('html', ExtractProseHTMLConverter)

    config.addLabel('extract-prose-part', {
      en: 'Extract: Prose',
    })
    config.addLabel('extract-prose-chapter', {
      en: 'Extract: Prose',
    })
  },
}
