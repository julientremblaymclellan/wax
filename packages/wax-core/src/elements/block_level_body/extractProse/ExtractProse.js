import { TextBlock } from 'substance'

class ExtractProse extends TextBlock {}

ExtractProse.type = 'extract-prose'

export default ExtractProse
