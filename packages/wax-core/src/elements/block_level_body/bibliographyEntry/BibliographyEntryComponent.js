import { TextBlockComponent } from 'substance'

class BibliographyEntryComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)
    return el.addClass('sc-bibliography-entry')
  }
}

export default BibliographyEntryComponent
