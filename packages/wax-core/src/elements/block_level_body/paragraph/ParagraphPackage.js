import Paragraph from './Paragraph'
import ParagraphComponent from './ParagraphComponent'
import ParagraphHTMLConverter from './ParagraphHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'paragraph',
  configure: function(config) {
    config.addNode(Paragraph)
    config.addComponent(Paragraph.type, ParagraphComponent)
    config.addConverter('html', ParagraphHTMLConverter)
    config.addCommand('paragraph-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'text-types-part',
    })
    config.addCommand('paragraph-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'text-types-chapter',
    })
    config.addLabel('paragraph-chapter', {
      en: 'Paragraph',
    })
    config.addLabel('paragraph-part', {
      en: 'Paragraph',
    })
    // config.addIcon('paragraph', { fontawesome: 'fa-paragraph' })
    config.addKeyboardShortcut('CommandOrControl+Alt+0', {
      command: 'paragraph',
    })
  },
  Paragraph: Paragraph,
  ParagraphComponent: ParagraphComponent,
  ParagraphHTMLConverter: ParagraphHTMLConverter,
}
