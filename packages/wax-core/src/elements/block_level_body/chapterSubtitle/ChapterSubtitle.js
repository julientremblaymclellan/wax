import { TextBlock } from 'substance'

class ChapterSubtitle extends TextBlock {}

ChapterSubtitle.type = 'chapter-subtitle'

export default ChapterSubtitle
