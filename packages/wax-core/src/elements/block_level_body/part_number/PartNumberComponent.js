import { TextBlockComponent } from 'substance'

class PartNumberComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-part-number')
  }
}

export default PartNumberComponent
