import Heading from './Heading'
import HeadingComponent from './HeadingComponent'
import HeadingMacro from './HeadingMacro'
import HeadingHTMLConverter from './HeadingHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'heading',
  configure: function(config) {
    config.addNode(Heading)
    config.addComponent(Heading.type, HeadingComponent)
    config.addConverter('html', HeadingHTMLConverter)

    config.addCommand('heading1-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 1 },
      commandGroup: 'text-display-part',
    })
    config.addCommand('heading2-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 2 },
      commandGroup: 'text-display-part',
    })
    config.addCommand('heading3-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 3 },
      commandGroup: 'text-display-part',
    })
    config.addCommand('heading1-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 1 },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('heading2-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 2 },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('heading3-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 3 },
      commandGroup: 'text-display-chapter',
    })
    // config.addKeyboardShortcut('CommandOrControl+Alt+1', {
    //   command: 'heading1',
    // })
    // config.addKeyboardShortcut('CommandOrControl+Alt+2', {
    //   command: 'heading2',
    // })
    // config.addKeyboardShortcut('CommandOrControl+Alt+3', {
    //   command: 'heading3',
    // })

    config.addCommand('heading1-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 1 },
      commandGroup: 'front-matter-b',
    })
    config.addCommand('heading2-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 2 },
      commandGroup: 'front-matter-b',
    })

    config.addCommand('heading1-back', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 1 },
      commandGroup: 'text-display-back',
    })
    config.addCommand('heading2-back', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 2 },
      commandGroup: 'text-display-back',
    })
    config.addCommand('heading3-back', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 3 },
      commandGroup: 'text-display-back',
    })

    config.addLabel('heading1-part', {
      en: 'Heading 1',
    })
    config.addLabel('heading2-part', {
      en: 'Heading 2',
    })
    config.addLabel('heading3-part', {
      en: 'Heading 3',
    })

    config.addLabel('heading1-chapter', {
      en: 'Heading 1',
    })
    config.addLabel('heading2-chapter', {
      en: 'Heading 2',
    })
    config.addLabel('heading3-chapter', {
      en: 'Heading 3',
    })

    config.addLabel('heading1-front', {
      en: 'Heading 1',
    })
    config.addLabel('heading2-front', {
      en: 'Heading 2',
    })

    config.addLabel('heading1-back', {
      en: 'Heading 1',
    })
    config.addLabel('heading2-back', {
      en: 'Heading 2',
    })
    config.addLabel('heading3-back', {
      en: 'Heading 3',
    })
    // config.addIcon('heading1', { fontawesome: 'fa-header' })
    // config.addIcon('heading2', { fontawesome: 'fa-header' })
    // config.addIcon('heading3', { fontawesome: 'fa-header' })
  },
  Heading,
  HeadingComponent,
  HeadingHTMLConverter,
  HeadingMacro,
}
