import ChapterTitle from './ChapterTitle'
import ChapterTitleComponent from './ChapterTitleComponent'
import ChapterTitleHTMLConverter from './ChapterTitleHTMLConverter'
import ChapterTitleProvider from './ChapterTitleProvider'
import ChapterTitleCommand from './ChapterTitleCommand'

export default {
  name: 'chapter-title',
  configure: config => {
    config.addNode(ChapterTitle)

    config.addComponent(ChapterTitle.type, ChapterTitleComponent)
    config.addConverter('html', ChapterTitleHTMLConverter)
    config.addCommand('chapter-title-part', ChapterTitleCommand, {
      spec: { type: 'chapter-title' },
      commandGroup: 'text-display-part',
    })
    config.addCommand('chapter-title-chapter', ChapterTitleCommand, {
      spec: { type: 'chapter-title' },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('front-chapter-title', ChapterTitleCommand, {
      spec: { type: 'chapter-title' },
      commandGroup: 'front-matter-a',
    })
    config.addLabel('chapter-title-part', {
      en: 'Title',
    })
    config.addLabel('chapter-title-chapter', {
      en: 'Title',
    })
    config.addLabel('front-chapter-title', {
      en: 'Title',
    })

    config.addProvider('chapterTitleProvider', ChapterTitleProvider)
  },
}
