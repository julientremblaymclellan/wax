import ParagraphContd from './ParagraphContd'
import ParagraphContdComponent from './ParagraphContdComponent'
import ParagraphContdHTMLConverter from './ParagraphContdHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'paragraph-contd',
  configure: function(config) {
    config.addNode(ParagraphContd)
    config.addComponent(ParagraphContd.type, ParagraphContdComponent)
    config.addConverter('html', ParagraphContdHTMLConverter)
    config.addCommand('paragraph-contd-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph-contd' },
      commandGroup: 'text-types-part',
    })
    config.addLabel('paragraph-contd-part', {
      en: `General Text Cont'd`,
    })
    config.addCommand('paragraph-contd-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph-contd' },
      commandGroup: 'text-types-chapter',
    })
    config.addLabel('paragraph-contd-chapter', {
      en: `General Text Cont'd`,
    })
    // config.addIcon('paragraph-contd', { fontawesome: 'fa-paragraph' })
    config.addKeyboardShortcut('CommandOrControl+Alt+1', {
      command: 'paragraph-contd',
    })
  },
  ParagraphContd: ParagraphContd,
  ParagraphContdComponent: ParagraphContdComponent,
  ParagraphContdHTMLConverter: ParagraphContdHTMLConverter,
}
