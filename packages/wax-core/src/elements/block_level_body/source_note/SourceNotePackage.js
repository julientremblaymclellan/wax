import SourceNote from './SourceNote'
import SourceNoteComponent from './SourceNoteComponent'
import SourceNoteHTMLConverter from './SourceNoteHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'source-note',
  configure: config => {
    config.addNode(SourceNote)
    config.addCommand('source-note-types-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-types-part',
    })
    config.addCommand('source-note-types-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-types-chapter',
    })
    config.addCommand('source-note-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-display-part',
    })
    config.addCommand('source-note-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('source-note-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'front-matter-b',
    })
    config.addComponent(SourceNote.type, SourceNoteComponent)
    config.addConverter('html', SourceNoteHTMLConverter)

    // config.addIcon('source-note', { fontawesome: 'fa-book' })
    config.addLabel('source-note-types-part', {
      en: 'Source Note',
    })
    config.addLabel('source-note-types-chapter', {
      en: 'Source Note',
    })
    config.addLabel('source-note-part', {
      en: 'Source Note',
    })
    config.addLabel('source-note-chapter', {
      en: 'Source Note',
    })
    config.addLabel('source-note-front', {
      en: 'Source Note',
    })
  },
}
