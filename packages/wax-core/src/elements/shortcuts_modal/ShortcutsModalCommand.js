import { Command } from 'substance'

class ShortcutsModalCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      if (editorProps.editing === 'selection') newState.disabled = true
    }

    return newState
  }

  execute(params) {
    const surface = params.editorSession.surfaceManager.getSurface('main')
    surface.context.editor.emit('toggleModal', 'shortCutsModal')
  }
}

export default ShortcutsModalCommand
