import { Tool } from 'substance'

class ShortcutsModalTool extends Tool {}
ShortcutsModalTool.type = 'shortcuts-modal'

export default ShortcutsModalTool
