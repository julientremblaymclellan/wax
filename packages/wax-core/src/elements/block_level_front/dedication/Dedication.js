import { TextBlock } from 'substance'

class Dedication extends TextBlock {}

Dedication.type = 'dedication'

export default Dedication
