import Signature from './Signature'
import SignatureComponent from './SignatureComponent'
import SignatureHTMLConverter from './SignatureHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'signature',
  configure: config => {
    config.addNode(Signature)
    config.addComponent(Signature.type, SignatureComponent)
    config.addConverter('html', SignatureHTMLConverter)

    config.addCommand('signature', WaxSwitchTextTypeCommand, {
      spec: { type: 'signature' },
      commandGroup: 'front-matter-c',
    })

    config.addLabel('signature', {
      en: 'Signature',
    })
  },
  Signature: Signature,
  SignatureComponent: SignatureComponent,
  SignatureHTMLConverter: SignatureHTMLConverter,
}
