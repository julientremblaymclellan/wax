import { TextBlock } from 'substance'

class Signature extends TextBlock {}

Signature.type = 'signature'

export default Signature
