import { TextBlock } from 'substance'

class SeriesEditor extends TextBlock {}

SeriesEditor.type = 'series-editor'

export default SeriesEditor
