import { last } from 'lodash'
import { Command } from 'substance'

import {
  containsImage,
  containsList,
  createContainerSelection,
  getContainer,
  isListItem,
  sliceListsAccordingToSelection,
  toggleList,
  transformListToParagraphs,
  transactionHelpers,
} from '../../helpers/ListHelpers'

export default class UCPListCommand extends Command {
  getCommandState(params) {
    let editorSession = this._getEditorSession(params)
    let doc = editorSession.getDocument()
    let sel = this._getSelection(params)

    if (params.surface && params.surface.containerId !== 'main') {
      return {
        disabled: true,
      }
    }

    if (params.surface && !params.surface.context.editor.props.mode.styling) {
      return {
        disabled: true,
      }
    }

    if (sel && sel.isPropertySelection()) {
      let path = sel.path
      let node = doc.get(path[0])
      if (node) {
        if (node.isListItem()) {
          let level = node.getLevel()
          let list = node.getParent()
          let listType = list.getListType(level)
          let active = listType === this.config.spec.listType
          let action = active ? 'toggleList' : 'setListType'
          let listId = list.id
          return {
            disabled: false,
            active,
            action,
            listId,
            level,
          }
        } else if (node.isText() && node.isBlock) {
          return {
            disabled: false,
            action: 'switchTextType',
          }
        }
      }
    } else if (sel && sel.isContainerSelection()) {
      return {
        disabled: false,
        action: 'switchTextType',
      }
    }
    return { disabled: true }
  }

  execute(params) {
    const { selection, surface } = transactionHelpers(params)
    const { commandState } = params
    const { disabled, action } = commandState
    const { editorSession } = params

    if (disabled) return

    switch (action) {
      case 'toggleList': {
        // editorSession.transaction(
        //   tx => {
        //     tx.toggleList()
        //   },
        //   { action: 'toggleList' },
        // )
        break
      }
      case 'setListType': {
        const { listId, level } = commandState
        editorSession.transaction(
          tx => {
            let list = tx.get(listId)
            list.setListType(level, this.config.spec.listType)
          },
          { action: 'setListType' },
        )
        break
      }
      case 'switchTextType': {
        this.applyMixedTransformations(params, selection)
        // editorSession.transaction(
        //   tx => {
        //     tx.toggleList({ listType: this.config.spec.listType })
        //   },
        //   { action: 'toggleList' },
        // )
        break
      }
      default:
      //
    }
  }
  applyMixedTransformations(params, selection) {
    const { editorSession } = transactionHelpers(params)

    const isStartSelectionListItem = isListItem(params, selection.start.path[0])
    const isEndSelectionListItem = isListItem(params, selection.end.path[0])
    const lists = sliceListsAccordingToSelection(params, selection)

    editorSession.transaction(tx => {
      const options = { config: this.config }
      const paragraphs = transformListToParagraphs(params, tx, lists, options)

      const startPath = isStartSelectionListItem
        ? paragraphs[0]
        : selection.start.path[0]

      const endPath = isEndSelectionListItem
        ? last(paragraphs)
        : selection.end.path[0]

      const sel = createContainerSelection(
        params,
        selection,
        tx,
        startPath,
        endPath,
      )
      const propertySelections = sel.splitIntoPropertySelections()

      this.transactionsForPropertyTransformation(params, tx, propertySelections)
    })
  }
  transactionsForPropertyTransformation(params, tx, propertySelections) {
    const { containerId, doc } = transactionHelpers(params)

    propertySelections.forEach((property, index) => {
      tx.setSelection({
        type: 'property',
        path: property.path,
        startOffset: 0,
        endOffset: 0,
        containerId,
      })

      if (index === 0) {
        const options = { config: this.config }
        toggleList(tx, params, options)
      } else {
        const container = getContainer(params)

        const currentPropertyNode = doc.get(property.start.path[0])
        const previousPropertyNode = doc.get(
          propertySelections[index - 1].start.path[0],
        )

        // HACK
        // Because tx.toggleList does not return the node that has just been
        // created, we do not know the node's position in the document.
        let currentNodePosition = 1

        if (currentPropertyNode) {
          currentNodePosition = container.getPosition(currentPropertyNode.id)
        }

        const previousNode = container.getNodeAt(currentNodePosition - 1)

        // TODO -- remove image reference
        if (
          (previousNode && previousNode.type === 'image') ||
          // (previousNode && previousNode.isText) ||
          (currentPropertyNode &&
            currentPropertyNode.content === '' &&
            previousPropertyNode &&
            previousPropertyNode.content === '')
        ) {
          const options = { config: this.config }
          toggleList(tx, params, options)
          return
        }

        tx.deleteCharacter('left')
        tx.break()
      }
    })
  }
  applyPropertyTransformations(params, propertySelections) {
    const { editorSession } = params

    editorSession.transaction(tx => {
      this.transactionsForPropertyTransformation(params, tx, propertySelections)
    })
  }
}
