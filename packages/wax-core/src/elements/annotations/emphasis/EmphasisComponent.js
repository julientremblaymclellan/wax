import { AnnotationComponent } from 'substance'

class EmphasisComponent extends AnnotationComponent {
  getTagName() {
    return 'em'
  }
}

export default EmphasisComponent
