import { PropertyAnnotation } from 'substance'

class TrackChangeFormat extends PropertyAnnotation {}

TrackChangeFormat.schema = {
  status: { type: 'string' },
  addedType: { type: 'array', default: [] },
  oldType: { type: 'array', default: [] },
  user: {
    type: 'object',
  },
}

TrackChangeFormat.type = 'track-change-format'

export default TrackChangeFormat
