const getSelection = surface => {
  return surface.domSelection.getSelection()
}

const isSelectionCollapsed = options => {
  const { selection } = options
  const isCollapsed = selection.isCollapsed()
  return isCollapsed
}

// TODO -- refactor this and isAnnotationContainedWithinSelection into one
const isSelectionContainedWithin = (options, annotation, strict) => {
  const selection = getSelection(options.surface)

  const annotationSelection = annotation.getSelection()

  return annotationSelection.contains(selection, strict)
}

const moveCursorTo = (options, point) => {
  const { surface } = options
  const selection = getSelection(surface)

  // TODO -- use substance's selection.collapse(direction)
  if (point === 'start') {
    selection.end.offset = selection.start.offset
  } else if (point === 'end') {
    selection.start.offset = selection.end.offset
  } else {
    selection.start.offset = point
    selection.end.offset = point
  }

  surface.editorSession.setSelection(selection)
}

const setSelectionPlusOne = (options, direction) => {
  const { selection, surface } = options

  if (direction === 'left') selection.start.offset -= 1
  if (direction === 'right') selection.end.offset += 1

  surface.editorSession.setSelection(selection)

  return selection
}

const updateSelection = (options, selection, startOffset, endOffset) => {
  const { surface } = options

  selection.start.offset = startOffset
  selection.end.offset = endOffset

  surface.editorSession.setSelection(selection)
  return selection
}

export {
  getSelection,
  isSelectionCollapsed,
  isSelectionContainedWithin,
  moveCursorTo,
  setSelectionPlusOne,
  updateSelection,
}
