import { platform } from 'substance'

import TrackChange from './TrackChange'
import TrackChangeCommand from './TrackChangeCommand'
import TrackChangeComponent from './TrackChangeComponent'
import TrackChangeHTMLConverter from './TrackChangeHTMLConverter'
import TrackChangeControlTool from './TrackChangeControlTool'
import TrackChangeControlViewTool from './TrackChangeControlViewTool'
import TrackChangeControlCommand from './TrackChangeControlCommand'
import TrackChangeControlViewCommand from './TrackChangeControlViewCommand'
import TrackChangesProvider from './TrackChangesProvider'
import TrackChangeAcceptCommand from './TrackChangeAcceptCommand'
import TrackChangeRejectCommand from './TrackChangeRejectCommand'
import TrackChangeAcceptControlTool from './TrackChangeAcceptControlTool'
import TrackChangeRejectControlTool from './TrackChangeRejectControlTool'

import TrackChangePreviousControlTool from './TrackChangePreviousControlTool'
import TrackChangeNextControlTool from './TrackChangeNextControlTool'
import TrackChangePreviousNextCommand from './TrackChangePreviousNextCommand'

// Track Formating

import TrackChangeFormat from './track_formating/TrackChangeFormat'
import TrackChangeFormatComponent from './track_formating/TrackChangeFormatComponent'
import TrackChangeFormatHTMLConverter from './track_formating/TrackChangeFormatHTMLConverter'

export default {
  name: 'track-change',
  configure: (config, { toolGroup }) => {
    config.addNode(TrackChange)
    config.addComponent(TrackChange.type, TrackChangeComponent)
    config.addConverter('html', TrackChangeHTMLConverter)

    config.addCommand(TrackChange.type, TrackChangeCommand, {
      nodeType: TrackChange.type,
    })

    // Toggle and view toggle
    config.addCommand('track-change-enable', TrackChangeControlCommand, {
      commandGroup: 'track-change-controls',
    })
    config.addTool('track-change-enable', TrackChangeControlTool)
    config.addLabel('track-change-enable', {
      en: 'Toggle recording changes',
    })

    config.addCommand(
      'track-change-toggle-view',
      TrackChangeControlViewCommand,
      {
        commandGroup: 'track-change-controls',
      },
    )
    config.addTool('track-change-toggle-view', TrackChangeControlViewTool)

    config.addLabel('track-change-toggle-view', {
      en: 'Toggle tracked changes view',
    })

    // Accept / Reject & Previous / Next

    config.addCommand('track-change-accept', TrackChangeAcceptCommand, {
      nodeTypes: [TrackChange.type, TrackChangeFormat.type],
      commandGroup: 'track-change-controls',
      actionType: 'accept',
    })
    config.addTool('track-change-accept', TrackChangeAcceptControlTool)
    config.addLabel('track-change-accept', {
      en: 'Accept change',
    })

    config.addCommand('track-change-reject', TrackChangeRejectCommand, {
      nodeTypes: [TrackChange.type, TrackChangeFormat.type],
      commandGroup: 'track-change-controls',
      actionType: 'reject',
    })
    config.addTool('track-change-reject', TrackChangeRejectControlTool)
    config.addLabel('track-change-reject', {
      en: 'Reject change',
    })

    config.addCommand('track-change-previous', TrackChangePreviousNextCommand, {
      nodeTypes: [TrackChange.type, TrackChangeFormat.type],
      commandGroup: 'track-change-controls',
      actionType: 'previous',
    })
    config.addTool('track-change-previous', TrackChangePreviousControlTool)
    config.addLabel('track-change-previous', {
      en: 'Go to previous change',
    })

    config.addCommand('track-change-next', TrackChangePreviousNextCommand, {
      nodeTypes: [TrackChange.type, TrackChangeFormat.type],
      commandGroup: 'track-change-controls',
      actionType: 'next',
    })
    config.addTool('track-change-next', TrackChangeNextControlTool)
    config.addLabel('track-change-next', {
      en: 'Go to next change',
    })

    // Formating
    config.addNode(TrackChangeFormat)
    config.addComponent(TrackChangeFormat.type, TrackChangeFormatComponent)
    config.addConverter('html', TrackChangeFormatHTMLConverter)
    // Shortcuts
    let controllerKey = 'ctrl'
    let altKey = 'alt'
    if (platform.isMac) {
      controllerKey = 'cmd'
      altKey = 'ctrl'
    }

    config.addKeyboardShortcut(`${controllerKey}+y`, {
      description: 'enable / disable track changes',
      command: 'track-change-enable',
    })

    config.addKeyboardShortcut(`${controllerKey}+shift+y`, {
      description: 'turn on / off track changes view',
      command: 'track-change-toggle-view',
    })

    config.addKeyboardShortcut(`${altKey}+a`, {
      description: 'accept tracked change(s)',
      command: 'track-change-accept',
    })

    config.addKeyboardShortcut(`${altKey}+r`, {
      description: 'reject tracked change(s)',
      command: 'track-change-reject',
    })

    config.addKeyboardShortcut(`${controllerKey}+${altKey}+n`, {
      description: 'go to next tracked change',
      command: 'track-change-next',
    })

    config.addKeyboardShortcut(`${controllerKey}+${altKey}+p`, {
      description: 'go to previous tracked change',
      command: 'track-change-previous',
    })

    config.addProvider('trackChangesProvider', TrackChangesProvider)
  },
}
