import { Command } from 'substance'
import TrackChange from './TrackChange'
// TODO -- why command and not annotation command?

class TrackChangeControlCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    setTimeout(() => {
      if (params.surface && params.surface.props.trackChanges) {
        TrackChange.autoExpandRight = true
      } else {
        TrackChange.autoExpandRight = false
      }
    })

    return newState
  }

  execute(params, context) {
    const surface = context.surfaceManager.getSurface('main')

    /*
      TODO -- MAJOR HACK
      Get whether track changes is on from the surface.
      Then send an update to the surface.
      The surface does not have a handler for this, so it will just send it
      upwards, ie. to the editor, which does have a handler
      (we don't have access to the editor here)
    */
    const trackChangesOn = surface.props.trackChanges
    surface.send('update', { trackChanges: !trackChangesOn })
    surface.rerender()
    // put the cursor back where it was on the surface
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    }

    return true
  }
}

TrackChangeControlCommand.type = 'track-change-enable'

export default TrackChangeControlCommand
