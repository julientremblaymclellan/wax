import { Tool } from 'substance'

class TrackChangeControlViewTool extends Tool {
  getClassNames() {
    return 'sm-target-track-change-toggle-view'
  }

  renderButton($$) {
    let buttonText = 'Hide changes'
    if (!this.getViewMode()) buttonText = 'Show changes'

    const el = super
      .renderButton($$)
      .append(buttonText)
      .addClass('track-changes-view-active')
    return el.attr('data-clickable', true)
  }

  getSurface() {
    const surfaceManager = this.context.surfaceManager
    const containerId = this.context.controller.props.containerId
    return surfaceManager.getSurface(containerId)
  }

  getViewMode() {
    const editor = this.context.editor
    const { trackChangesView } = editor.state

    return trackChangesView
  }
}

TrackChangeControlViewTool.type = 'track-change-toggle-view'

export default TrackChangeControlViewTool
