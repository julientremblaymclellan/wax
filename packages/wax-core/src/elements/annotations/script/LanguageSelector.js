import { map, forEach, includes } from 'lodash'
import { Component, FontAwesomeIcon as Icon } from 'substance'

class LanguageSelector extends Component {
  render($$) {
    const languages = [
      { name: 'Javascript', value: 'javascript' },
      { name: 'CSS', value: 'css' },
      { name: 'HTML', value: 'htmlmixed' },
    ]

    const listItems = map(languages, entry => {
      let option = ''
      if (entry.value === this.props.node.language) {
        option = $$('option')
          .attr('value', entry.value)
          .attr('selected', true)
          .append(entry.name)
      } else {
        option = $$('option')
          .attr('value', entry.value)
          .append(entry.name)
      }

      return option
    })

    const languageDropDown = $$('select')
      .attr('id', 'lang')
      .append(listItems)
      .on('change', this.props.onSelect)
      .on('change', this.changeState)

    const cog = $$(Icon, { icon: 'fa-cog' })
      .addClass('languange-selector-icon')
      .on('click', this.selectCodingLanguage)

    const removeCodeBlock = $$(Icon, { icon: 'fa-times' })
      .addClass('languange-selector-icon')
      .on('click', this.removeCodeBlock)

    const languageSelector = $$('div')
      .addClass('languange-selector-container')
      .append(cog, removeCodeBlock)

    if (this.state.dropDownOpen) languageSelector.append(languageDropDown)

    return languageSelector
  }

  changeState() {
    this.extendState({ dropDownOpen: !this.state.dropDownOpen })
  }

  removeCodeBlock() {
    const containers = this.getAllAvailableSurfaces()
    const surfaceManager = this.context.editor.surfaceManager

    const focusedSurface = this.context.editor.surfaceManager.getFocusedSurface()
    let container = ''
    forEach(containers, cont => {
      if (includes(cont.nodes, this.props.node.id)) {
        container = cont
      }
    })

    // make sure if you delete code block immediately after inserting
    // to get focus back on Wax Surface
    if (typeof focusedSurface === 'undefined') {
      surfaceManager.surfaces[container.id].setFocusOnStartOfText()
    }

    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'node',
        containerId: container.id,
        nodeId: this.props.node.id,
      })
      tx.deleteSelection()
    })
  }

  getAllAvailableSurfaces() {
    const nodes = this.context.editorSession.document.getNodes()
    const containers = []

    forEach(nodes, node => {
      if (node._isContainer) {
        containers.push(node)
      }
    })
    return containers
  }

  selectCodingLanguage() {
    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'node',
        containerId: 'main',
        nodeId: this.props.node.id,
      })
    })
    this.extendState({ dropDownOpen: !this.state.dropDownOpen })
  }

  getInitialState() {
    return {
      dropDownOpen: false,
    }
  }
}

export default LanguageSelector
