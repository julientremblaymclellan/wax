import { Component } from 'substance'
import LanguageSelector from './LanguageSelector'

import CodeMirror from 'codemirror'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/mode/css/css.js'
import 'codemirror/mode/htmlmixed/htmlmixed.js'

import 'codemirror/keymap/sublime.js'

import 'codemirror/addon/hint/show-hint.js'
import 'codemirror/addon/hint/javascript-hint.js'

import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/cobalt.css'
import 'codemirror/addon/hint/show-hint.css'

class ScriptEditor extends Component {
  constructor(...props) {
    super(...props)
    this.changeLanguage = this.changeLanguage.bind(this)
    this.context.editor.on('codeBlock:created', this.moveFocus, this)
  }

  render($$) {
    let node = this.props.node
    let el = $$('div').addClass('sc-script-editor')
    el.append($$('div').ref('source'))

    const languageSelector = $$(LanguageSelector, {
      node: this.props.node,
      onSelect: this.changeLanguage,
    })

    el.addClass('editor-border').append(languageSelector)
    return el
  }

  // don't rerender because this would destroy code-mirror
  shouldRerender() {
    return false
  }

  changeLanguage() {
    const node = this.props.node
    const dropDown = document.getElementById('lang')
    const languageSelected = dropDown.options[dropDown.selectedIndex].value

    this.context.editorSession.transaction(function(tx) {
      tx.set([node.id, 'language'], languageSelected)
    })
    // remove previous instance
    this.editor
      .getWrapperElement()
      .parentNode.removeChild(this.editor.getWrapperElement())
    this.didMount()
  }

  didMount() {
    let editorSession = this.context.editorSession

    let node = this.props.node

    let editor = CodeMirror(this.refs.source.getNativeElement(), {
      value: node.source,
      lineNumbers: true,
      theme: 'cobalt',
      keyMap: 'sublime',
      extraKeys: { 'Ctrl-Space': 'autocomplete' },
      mode: this.props.node.language,
    })

    this.editor = editor
    editor.on('blur', this._updateModelOnBlur.bind(this))
    editor.on('paste', this.paste.bind(this), this)
  }

  moveFocus() {
    this.context.editorSession.setSelection(null)
    this.editor.focus()
  }

  paste() {
    this.context.editorSession.setSelection(null)
  }

  dispose() {
    this.context.editorSession.off(this)
    this.editor.removeOverlay()
  }

  _updateModelOnBlur() {
    let editor = this.editor
    let nodeId = this.props.node.id
    let source = editor.getValue()

    if (source !== this.props.node.source) {
      this.context.editorSession.transaction(function(tx) {
        tx.set([nodeId, 'source'], editor.getValue())
      })
    }
  }
}

export default ScriptEditor
