import { BlockNode } from 'substance'

class Script extends BlockNode {}

Script.schema = {
  type: 'script',
  language: { type: 'string', default: 'javascript' },
  source: 'text',
}

export default Script
