import { PropertyAnnotation, Fragmenter } from 'substance'

class Strong extends PropertyAnnotation {}

Strong.type = 'strong'
Strong.fragmentation = Fragmenter.ANY

export default Strong
