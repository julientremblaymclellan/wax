import { platform } from 'substance'

import Comment from './Comment'
import CommentBubble from './CommentBubble'
import CommentCommand from './CommentCommand'
import CommentComponent from './CommentComponent'
import CommentHTMLConverter from './CommentHTMLConverter'
import ResolvedCommentPackage from './ResolvedCommentPackage'
import CommentsProvider from './CommentsProvider'

export default {
  name: 'comment',
  configure: (
    config,
    {
      disableCollapsedCursor, // TODO -- should delete?
      toolGroup,
    },
  ) => {
    config.import(ResolvedCommentPackage)

    config.addNode(Comment)

    config.addComponent(Comment.type, CommentComponent)
    config.addConverter('html', CommentHTMLConverter)

    config.addCommand(Comment.type, CommentCommand, {
      disableCollapsedCursor, // TODO -- same as above
      nodeType: Comment.type,
      // spec: { type: 'comment' },
      commandGroup: 'comment',
    })

    config.addTool('comment', CommentBubble)

    config.addIcon('comment', { fontawesome: 'fa-comment-o' })
    config.addLabel('comment', {
      en: 'Comment',
    })

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'

    config.addKeyboardShortcut(`${controllerKey}+m`, {
      command: Comment.type,
    })

    config.addProvider('commentsProvider', CommentsProvider)
    Comment.autoExpandRight = false
  },
}
