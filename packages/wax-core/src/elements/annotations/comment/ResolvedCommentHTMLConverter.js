/* eslint no-param-reassign: "off" */

export default {
  type: 'resolved-comment',
  tagName: 'resolved-comment',

  import: (element, node) => {
    node.data = JSON.parse(element.attr('data'))
  },

  export: (node, element) => {
    element.setAttribute('data', JSON.stringify(node.data))
  },
}
