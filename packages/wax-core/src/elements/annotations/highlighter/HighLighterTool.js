import { forEach } from 'lodash'
import { Tool, FontAwesomeIcon as Icon, documentHelpers } from 'substance'

class HighLighterTool extends Tool {
  constructor(...args) {
    super(...args)
    this.colors = [
      { name: 'yellow', code: '#fdf47D' },
      { name: 'green', code: '#befdc4' },
      { name: 'blue', code: '#c5fff9' },
    ]
  }
  render($$) {
    const defaultColor = this.colors[0]

    const dropDownCarret = $$(Icon, { icon: 'fa-caret-down' })
      .addClass('icon-down')
      .on('click', this.toggleDropdown)

    let el = $$('div')
      .append(
        $$('div')
          .addClass(defaultColor.name)
          .addClass('color-box')
          .attr('title', `Add ${defaultColor.name} highlighting`)
          .on('click', () => this.addHighLighting(defaultColor.name)),
      )
      .addClass('highlighter-wrapper')
      .append(dropDownCarret)

    if (this.props.commandState.disabled) el.addClass('sm-disabled')

    if (this.state.open) {
      el.addClass('sm-open')
      const clearButton = $$('button')
        .append(
          $$('div')
            .addClass('clear-color')
            .addClass('color-box'),
        )
        .attr('title', 'remove highlighting')
        .on('click', this.removeHighLighting)
      // dropdown options
      let options = $$('div')
        .addClass('se-options')
        .ref('options')
      forEach(this.colors, (color, index) => {
        if (index < 1) return
        let button = $$('button')
          .addClass('se-option sm-' + color.name)
          .attr('data-type', color)
          .append(
            $$('div')
              .addClass(color.name)
              .addClass('color-box')
              .attr('title', `Add ${color.name} highlighting`),
          )
          .on('click', () => this.addHighLighting(color.name))
        options.append(button)
      })
      options.append(clearButton)
      el.append(options)
    }
    return el
  }

  removeHighLighting() {
    const editorSession = this.getEditorSession()
    const highlightings = this.getExistingHighlightings()

    forEach(highlightings, highlighting => {
      editorSession.transaction(tx => {
        tx.delete(highlighting.id)
      })
    })
  }

  toggleDropdown() {
    this.extendState({
      open: !this.state.open,
    })
  }

  addHighLighting(color) {
    const editorSession = this.getEditorSession()
    const selection = editorSession.getSelection()

    if (selection.isContainerSelection()) {
      this.createContainerHighLighter()
      return
    }

    const highlightings = this.getExistingHighlightings()
    if (highlightings.length > 0) this.removeHighLighting()

    if (highlightings.length === 1 && selection.isCollapsed()) {
      editorSession.transaction(tx => {
        tx.create({
          color,
          type: 'highlighter',
          path: highlightings[0].path,
          start: highlightings[0].start,
          end: highlightings[0].end,
        })
      })
      return
    }

    editorSession.transaction(tx => {
      tx.create({
        selection,
        color,
        type: 'highlighter',
        path: selection.path,
        start: selection.start,
        end: selection.end,
      })
    })
  }

  getExistingHighlightings() {
    const editorSession = this.getEditorSession()
    const highlightings = documentHelpers.getPropertyAnnotationsForSelection(
      editorSession.getDocument(),
      editorSession.getSelection(),
      { type: 'highlighter' },
    )

    return highlightings
  }

  createContainerHighLighter() {
    this.context.editor.emit('displayNotification', () => {
      return 'Multi block transformations are not yet supported for highlighter.'
    })
  }

  getEditorSession() {
    return this.context.editorSession
  }

  getInitialState() {
    return {
      open: false,
    }
  }
}

export default HighLighterTool
