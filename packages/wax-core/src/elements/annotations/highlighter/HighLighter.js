import { PropertyAnnotation } from 'substance'

class HighLighter extends PropertyAnnotation {}

HighLighter.schema = {
  type: 'highlighter',
  color: {
    type: 'string',
    default: '#fdf47d',
  },
}

export default HighLighter
