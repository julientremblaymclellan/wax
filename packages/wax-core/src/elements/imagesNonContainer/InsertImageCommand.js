import { includes } from 'lodash'
import { Command } from 'substance'

class ImageCommand extends Command {
  constructor() {
    super({ name: 'insert-image' })
  }

  getCommandState(params) {
    let sel = params.selection
    let surface = params.surface
    let newState = {
      disabled: true,
      active: false,
    }

    if (surface && surface.props.editing === 'selection') {
      newState.disabled = true
      return newState
    }

    if (
      sel &&
      !sel.isNull() &&
      !sel.isCustomSelection() &&
      surface &&
      surface.isContainerEditor() &&
      (includes(surface.props.textCommands, 'full') ||
        includes(surface.props.textCommands, this.name))
    ) {
      newState.disabled = false
    }
    return newState
  }

  execute(params) {
    const editorSession = params.editorSession

    editorSession.transaction(tx => {
      params.files.forEach(file => {
        this._insertImage(tx, file)
      })
    })

    editorSession.fileManager.sync()
  }

  _insertImage(tx, file) {
    let imageFile = tx.create({
      type: 'file',
      fileType: 'image',
      mimeType: file.type,
      sourceFile: file,
    })

    tx.insertBlockNode({
      type: 'image',
      imageFile: imageFile.id,
    })
  }
}

export default ImageCommand
