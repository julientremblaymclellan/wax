/*
 * HTML converter for Paragraphs.
 */
export default {
  type: 'image',
  tagName: 'figure',

  import: function(el, node, converter) {
    let imageFile = converter.getDocument().create({
      id: 'file-' + node.id,
      type: 'file',
      fileType: 'image',
      url: el.attr('src'),
    })
    node.imageFile = imageFile.id
    node.caption = el.attr('figcaption')
  },

  export: function(node, el) {
    let imageFile = node.document.get(node.imageFile)
    el.attr('src', imageFile.getUrl())
    el.attr('figcaption', node.caption)
  },
}
