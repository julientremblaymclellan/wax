import { map, findKey, find, forEach } from 'lodash'

import { Component } from 'substance'

class InlineNoteComponent extends Component {
  didMount() {
    const provider = this.getProvider()
    provider.on('note:dragged', this.setNewReferences, this)
    provider.on('notes:updated', this.setNewReferences, this)
  }
  render($$) {
    const provider = this.getProvider()
    const notes = provider.entries
    const node = this.props.node

    const nullOption = $$('option')
      .attr('value', '')
      .append('')

    let el = $$('span')
      .append(`note  ${node.number}`)
      .on('click', this.createOptions)

    const listItems = map(notes, (note, index) => {
      let option = ''

      if (node.reference === note.id) {
        option = $$('option')
          .attr('value', node.reference)
          .attr('selected', true)
          .append(node.number)
      } else {
        option = $$('option')
          .attr('value', note.id)
          .append(index + 1)
      }

      return option
    })

    const options = $$('select')
      .attr('id', node.id)
      .append(nullOption)
      .append(listItems)
      .on('change', this.onSelect)

    if (
      (this.el && this.el.hasClass('drop-down-options')) ||
      node.reference === ''
    ) {
      el = options
    }

    return el
  }

  createOptions() {
    this.el.addClass('drop-down-options')
  }

  onSelect() {
    const editorSession = this.getEditorSession()
    const node = this.props.node
    const doc = editorSession.document

    const dropDown = document.getElementById(node.id)
    const noteSelected = dropDown.options[dropDown.selectedIndex].value
    const number = dropDown.options[dropDown.selectedIndex].text

    doc.set([node.id, 'reference'], noteSelected)
    doc.set([node.id, 'number'], number)
  }

  setNewReferences() {
    const provider = this.getProvider()
    const editorSession = this.getEditorSession()

    const notes = provider.entries
    const node = this.props.node
    const reference = node.reference
    const doc = editorSession.document
    const documentNodes = doc.getNodes()

    const arr = []
    forEach(documentNodes, node => {
      if (node.type === 'inline-note') {
        arr.push(node)
      }
    })

    if (arr.length === 0) return

    const noteFound = find(notes, note => note.id === reference)
    const keyFound = findKey(notes, note => note.id === reference)
    const index = parseInt(keyFound, 10) + 1
    if (noteFound) {
      doc.set([node.id, 'reference'], reference)
      doc.set([node.id, 'number'], index)
    } else {
      doc.set([node.id, 'reference'], '')
      doc.set([node.id, 'number'], '')

      // editorSession.transaction(tx => {
      //   tx.delete(node.id)
      // })
    }
    this.rerender()
  }

  getProvider() {
    return this.context.notesProvider
  }

  getEditorSession() {
    return this.context.editorSession
  }
}

export default InlineNoteComponent
