import { AnnotationTool } from 'substance'

class InlineNoteTool extends AnnotationTool {
  renderButton($$) {
    const el = super.renderButton($$)
    return el
  }
}
InlineNoteTool.type = 'inline-note'

export default InlineNoteTool
