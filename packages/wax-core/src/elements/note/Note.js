import { InlineNode } from 'substance'

class Note extends InlineNode {}

Note.schema = {
  type: 'note',
  disabled: {
    type: 'boolean',
    default: false,
  },
}

export default Note
