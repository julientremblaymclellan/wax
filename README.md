# Wax Editor

## Roadmap

26 September - 2 October 2018

Wax Inline note callout
Configurable tools

15-30 APRIL 2018

### Bug Fixing

* Find and Replace a single match and undo throws an Error (Status: Fixed)
* Undo / Redo Notes on certain Occasions throws an Error (Status: Fixed)
* Exiting an Unsaved chapter on Editoria, and clicking not save changes throughs an error when unsaved notes exist (Status: Pending)
* Undo A Note with track changes on happens in 2 click (first track-changed removed and then the note ) (Status: Pending)
* Opening an Empty Chapter Save is Enabled and modal is triggered if you try to go back without any change in the Editor (Status: Fixed)
* if Track Changes is on, user cannot remove an image (Status: Fixed)
* Remove additions on cut operation in track changes if done from the same user (Status: Fixed)
* With track Changes on if you accept a deleted note and undo throws an error (Status: Fixed)
* With track Changes on if you delete a whole paragraph and undo throws an error (Status: Fixed)

### Improved Features

* Improve Performance in typing , in find and replace input
* Removed unneeded events / listeners from notes

### New Features

* Navigate Through Notes with left and right arrow – Done
* Toggle on /off Native Spell Checker – Done
* Full screen mode – Done
* Track Spaces – Done
* Image Captions – Done
* Change note icon in the toolbar – Done
* Add keyboard shortcuts for accept & reject track changes – Done
* Small Caps – Done
* Add keyboard shortcuts for Special Characters

## Get up and running

Coming soon
